#include "looptime.h"
Looptime lp[lpTotalNumber];
void lp_init(Looptime *task,uint16_t interval,uint16_t offset){
	task->interval = interval;
	task->lasttime = HAL_GetTick()+offset;
}
int lp_task(Looptime *task){
	if(HAL_GetTick() > task->lasttime+task->interval){
	task->lasttime = HAL_GetTick();
	return 1;
	}else{
	return 0;
	}
}
