#include "lmx2491.h"
#include "lmxInit.h"
void lmx2491_write(SPI_HandleTypeDef *myspi,uint8_t Address_MSB,uint8_t Address_LSB,uint8_t Value){
	uint8_t data[] = {Address_MSB,Address_LSB,Value};
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_RESET);// SPI CS
	//HAL_Delay(1);
	HAL_SPI_Transmit(myspi,(uint8_t*)&data[0],3,0xFFFF);
	//HAL_Delay(1);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET);// SPI CS
}

uint8_t lmx2491_read(SPI_HandleTypeDef *myspi,uint8_t Address_MSB,uint8_t Address_LSB){
	uint8_t txdata[] = {Address_MSB|0x80,Address_LSB,0xFF};
	uint8_t rxdata[] = {0xFF,0xFF,0xFF};
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_RESET);// SPI CS
	HAL_SPI_TransmitReceive(myspi,(uint8_t*)&txdata[0],(uint8_t*)&rxdata[0],3,0xFFFF);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET);// SPI CS
	return rxdata[2];
}
void lmx2391_SPI_init(void){
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
		while(1){
		 HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
	 }
  }
}
void lmx2491_init(SPI_HandleTypeDef *myspi){
	int counter = 0;
	lmx2391_SPI_init();
	uint8_t dummy = 0xff;
	HAL_SPI_Transmit(myspi,(uint8_t*)&dummy,1,0);//dummy transmit
	HAL_Delay(250);	
	for(uint8_t i = 0;i<regcount;i++)
	{
		lmx2491_write(myspi,0x00,LMXINITREG[i][0],LMXINITREG[i][1]);
	};
	HAL_Delay(100);
	while(lmx2491_read(myspi,0x00,0x00) != 0x18){
		counter++;
		HAL_Delay(500);
		if(counter > 10){
			while(1){
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
			}
		}
	}
}
void lmx2491_init_test(SPI_HandleTypeDef *myspi){
	lmx2391_SPI_init();
	uint8_t dummy = 0xff;
	HAL_SPI_Transmit(myspi,(uint8_t*)&dummy,1,0);//dummy transmit
	HAL_Delay(100);	
	lmx2491_write(myspi,0x00,0x02,0x05);
	HAL_Delay(500);	
	lmx2491_write(myspi,0x00,0x02,0x01);
	HAL_Delay(10);	
	lmx2491_write(myspi,0x00,0x23,0x41);
	HAL_Delay(10);
	lmx2491_write(myspi,0x00,0x27,0x38);	
	HAL_Delay(10);	
//	lmx2491_write(myspi,0x00,0x24,0x00);
//	for(int i =0;i<5;i++){
//	lmx2491_write(myspi,0x00,0x24,0x06);
//  HAL_Delay(20);
//	lmx2491_write(myspi,0x00,0x24,0x02);
//  HAL_Delay(10);
//	}
	 HAL_Delay(250);
  uint8_t data2[] = {0x80,0x00,0x00};
	uint8_t rxdata2[] = {0xFF,0xFF,0xFF};
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_RESET);// SPI CS
	HAL_Delay(1);
	HAL_SPI_TransmitReceive(myspi,(uint8_t*)&data2[0],(uint8_t*)&rxdata2[0],3,0xFFFF);
	HAL_Delay(1);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET);// SPI CS
	HAL_Delay(10);
}
