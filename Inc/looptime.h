/* written by Wilson Wong 
*/
#ifndef __looptime
#define __looptime
#ifdef __cplusplus
 extern "C" {
#endif 
#include "stm32l4xx_hal.h"	
#include <stdlib.h> 
#define lpTotalNumber 7
typedef struct _tLooptime{
	uint16_t interval;
	uint32_t lasttime;
}Looptime;
extern Looptime lp[lpTotalNumber];
int lp_task(Looptime *task);
void lp_init(Looptime *task,uint16_t interval,uint16_t offset);
#ifdef __cplusplus
}
#endif	 
#endif
