//Please change you UART by changing (&huart6) and huart6 below.
#ifndef __commonwws
#define __commonwws
#ifdef __cplusplus
 extern "C" {
#endif 
#include "stdio.h"	 
#include "stm32l4xx_hal.h"
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart6;
void delayUS(uint32_t us);
struct __FILE {
    int dummy;
};
FILE __stdout;
int fputc(int ch, FILE *f) {
    /* Do your stuff here */
    /* Send your custom byte */
    /* Send byte to USART */
		//HAL_UART_Transmit(&huart1,(uint8_t*)&ch,1,2);
	 HAL_UART_Transmit_DMA(&huart1,(uint8_t*)&ch,1);
	 while(!__HAL_UART_GET_FLAG(&huart1,UART_FLAG_TC)){}
		/*HAL_UART_Transmit_DMA(&huart1,(uint8_t*)&ch,1);
		while(!__HAL_UART_GET_FLAG(&huart1,UART_FLAG_TC)){}*/
    /* If everything is OK, you have to return character written */
    return ch;
    /* If character is not correct, you can return EOF (-1) to stop writing */
    //return -1;
}
//#pragma push /* Save existing optimization level */
//#pragma O0   /* Optimization level now O3 */
/*void delayUS(uint32_t us) {
	volatile uint32_t counter = 19*us;
	while(counter--);
}*/
//#pragma pop /* Restore original optimization level */
void printc(char* data,float value){
printf("#%s$%f#\n",data,value);
}
#ifdef __cplusplus
}
#endif	 
#endif
