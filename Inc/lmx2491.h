#ifndef __lmx2491
#define __lmx2491
#ifdef __cplusplus
 extern "C" {
#endif 
#include "stm32l4xx_hal.h"
extern SPI_HandleTypeDef hspi1;
void lmx2491_init(SPI_HandleTypeDef *myspi);
void lmx2491_init_test(SPI_HandleTypeDef *myspi);
void lmx2391_SPI_init(void);
void lmx2491_write(SPI_HandleTypeDef *myspi,uint8_t Address_MSB,uint8_t Address_LSB,uint8_t Value);
uint8_t lmx2491_read(SPI_HandleTypeDef *myspi,uint8_t Address_MSB,uint8_t Address_LSB);
#ifdef __cplusplus
}
#endif	 
#endif
