#ifndef __bgt24
#define __bgt24
#ifdef __cplusplus
 extern "C" {
#endif 
#include "stm32l4xx_hal.h"
extern SPI_HandleTypeDef hspi1;
void bgt24_SPI_init(void);
void bgt24_init(SPI_HandleTypeDef *myspi);
#ifdef __cplusplus
}
#endif	 
#endif
